.PHONY: test build_base_adoc deploy_public clean_html

BASE_DIR = $(dir $(shell pwd))
DIR = $(notdir $(shell pwd))
ROOT_DIR = ${BASE_DIR}${DIR}
ADOC_DIR = ${ROOT_DIR}/adoc
ELB_SECURITY_POLICY_CHANGES = ${ADOC_DIR}/elb-security-policy-changes
SKC_ELB_MODULES = ${ELB_SECURITY_POLICY_CHANGES}/modules/skc_elb
PUBLIC_DIR = ${ROOT_DIR}/public


test:
	echo 'this is a test';
	echo ${BASE_DIR}
	echo ${DIR}
	echo ${ROOT_DIR}
	echo ${ADOC_DIR}

build_all: build_base_adoc build_elb_security_policy_changes build_skc_elb_modules

build_base_adoc:
	cd ${ADOC_DIR}; \
	pwd ; \
	docker run  -v ${ADOC_DIR}:/documents/ asciidoctor/docker-asciidoctor asciidoctor *.adoc; \


build_elb_security_policy_changes:
	cd ${ELB_SECURITY_POLICY_CHANGES}; \
	pwd; \
	docker run  -v ${ELB_SECURITY_POLICY_CHANGES}:/documents/ asciidoctor/docker-asciidoctor asciidoctor *.adoc; \


build_skc_elb_modules:
	cd ${SKC_ELB_MODULES} ; \
	pwd ; \
	docker run  -v ${SKC_ELB_MODULES}:/documents/ asciidoctor/docker-asciidoctor asciidoctor *.adoc; \


deploy_public:
	cd ${ADOC_DIR}; \
	pwd ; \
	cp -R . ../public; \
	find ${PUBLIC_DIR} -type f ! -name '*.html' -delete; 

clean_html:
	sudo find . -name "*.html" -type f -delete
