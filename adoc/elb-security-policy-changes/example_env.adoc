= example_env
:toclevels: 5
:source-highlighter: pygments
:pygments-style: emacs
:icons: font

[source, bash]
----
AWS_ACCESS_KEY_ID=xxxxx
AWS_SECRET_ACCESS_KEY=xxxx
AWS_DEFAULT_REGION=us-east-2
AWS_DEFAULT_OUTPUT=table
----
