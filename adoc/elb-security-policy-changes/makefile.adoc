= makefile
:toclevels: 5
:source-highlighter: pygments
:pygments-style: emacs
:icons: font

[source, makefile]
----

.PHONY: ter test

include .env
export

BASE_DIR = $(dir $(shell pwd))
DIR = $(notdir $(shell pwd))


test-env:
	env

test:
	echo $(BASE-DIR)$(DIR)$(WORKDIR)


# make tr cmd='-version'
cmd:
	terraform $(cmd) 


init:
	terraform init

validate:
	terraform validate

plan:
	terraform plan

apply:
	terraform apply

auto_apply:
	terraform apply -auto-approve

destroy:
	terraform destroy


list:
	terraform state list

providers:
	terraform providers

runinitial:
	export TF_LOG="TRACE" && \
	export TF_LOG_PATH="terraform_run_inital.log" && \
	terraform apply -auto-approve -var cert_to_use=0 -no-color 2>&1 | tee apply-init.txt

runone:
	export TF_LOG="TRACE" && \
	export TF_LOG_PATH="terraform_run_one.log" && \
	terraform apply -auto-approve -var cert_to_use=0 -no-color 2>&1 | tee apply-one.txt


runtwo:
	export TF_LOG="TRACE" && \
	export TF_LOG_PATH="terraform_run_two.log" && \
	terraform apply -auto-approve -var cert_to_use=1 -no-color 2>&1 | tee apply-two.txt

----
